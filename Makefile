PWD=$(shell pwd)
COMMANDS="init force_init"
LINKABLE_FILES=vimrc vim zshrc zsh

all:
	@echo "Select option: "$(COMMANDS)

init: $(LINKABLE_FILES)# Deploy dotfiles to ~/
	@for item in $(LINKABLE_FILES) ; do \
		if [ ! -e ~/.$$item ]; then \
			ln -vs $(PWD)/$$item $$HOME/.$$item ; \
		fi ; \
	done

force_init: $(LINKABLE_FILES) # Deploy dotfiles to ~/, erasing the old ones
	@for item in $(LINKABLE_FILES) ; do \
		ln -vsf $(PWD)/$$item $$HOME/.$$item ; \
	done
