# Enable Powerlevel10k instant prompt.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# The following lines were added by compinstall

#[set up history]
HISTFILE=~/.histfile
HISTSIZE=10000000
SAVEHIST=10000000
bindkey -e
setopt inc_append_history
setopt share_history
setopt HIST_IGNORE_DUPS
setopt COMPLETE_ALIASES

#[completion]
autoload -Uz compinit promptinit
compinit
promptinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' menu select=2
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion::complete:*' gain-privileges 1
#zstyle ':completion:*:descriptions' format '%U%F{cyan}%d%f%u'


# [No flowcontrol]
#stty -ixon
setopt noflowcontrol

# [Aliases]
alias cp='cp -iv'
alias rcp='rsync -v --progress'
alias rmv='rsync -v --progress --remove-source-files'
alias mv='mv -iv'
alias rm='rm -iv'
alias rmdir='rmdir -v'
alias ln='ln -v'
alias chmod="chmod -c"
alias chown="chown -c"
alias mkdir="mkdir -v"
alias grep='grep --colour=auto'
alias diff='diff --colour=auto'
alias egrep='egrep --colour=auto'
alias sudo='sudo -E'

export LANG="en_US.UTF-8"

if command -v colordiff > /dev/null 2>&1; then
    alias diff="colordiff -Nuar"
else
    alias diff="diff -Nuar"
fi

# if lsd is available use it
if [ -x "$(command -v lsd)" ]; then
    alias ls='lsd'
fi

# if neovim is available use it
if [ -x "$(command -v nvim)" ]; then
    alias vim='nvim'
fi

export LESS='-R --use-color -Dd+r$Du+b'

typeset -g -A key

autoload -Uz add-zle-hook-widget history-search-end

# [ Reconfigure Home, End and Delete ]
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
bindkey "\e[3~" delete-char

# [Guided history search]
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end

# [ Point to powerlevel10k ]
source $HOME/.zsh/powerlevel10k/powerlevel10k.zsh-theme

# [ Point to one of the p10k conf files ]
[[ ! -f ~/.zsh/old_p10k.zsh ]] || source ~/.zsh/old_p10k.zsh
