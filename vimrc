" Load plugins
if has('packages')
    packadd! vim-surround
endif

" tab is 4 spaces width
set shiftwidth=4
" tab using spaces
set softtabstop=4
set expandtab

" make some whitespace visible
let every_whitespace_visible=0
"" make every whitespace visible (situational use)
"let every_whitespace_visible=1

if every_whitespace_visible == 0
    set listchars=tab:>-,trail:~,extends:>,precedes:<
else
    set listchars=tab:>-,eol:$,trail:~,extends:>,precedes:<
endif
set list

if has('nvim')
    set guicursor=
    set mouse=
endif

" search before enter and show command
set incsearch
set showcmd

" dont show line number
set nonumber
"" show line number
"set number

" set tags to hidden file .tags
set tags=.tags

syntax on
" line at the bottom: 0-never, 1-with multiple windows, 2-always
set laststatus=2

" TODO: Setup cscope
" TODO: Setup colors so we can activate line numbers
" TODO: Setup Add syntax highlight configurations
